import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  word='';
  constructor(public navCtrl: NavController,private sppechRecogniser:SpeechRecognition) {

  }
  ionViewDidLoad(){
    // console.log()
    // console.log(this.sppechRecogniser.hasPermission());
    // console.log("recognition availabel",JSON.stringify(this.sppechRecogniser.hasPermission()));
    // console.log("recognition availabel",JSON.stringify(this.sppechRecogniser.isRecognitionAvailable()));
  }
  startRecognition(){
    // console.log("strart recognising");
    this.sppechRecogniser.startListening({
      language:'en',
      showPopup:true,
      matches:1
    }).subscribe(data=>{
      console.log(data);
      // return array
    },err=>{
      console.log(err);
      console.log("error recording ");
    })
  }
  stopRecognition(){
    console.log("Stop recognising");
    this.sppechRecogniser.stopListening()
  }

}
